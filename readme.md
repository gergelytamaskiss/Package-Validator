## About this project

<p align="center"><img src="https://github.com/gergelytamaskiss/Package-Validator/blob/master/resources/Screenshot%202017-11-05.png?raw=true"></p>

Validation of the created service packages made easier with this tool. Search can be made with different parameters (model, factory, build date). Returned query used to create shuffled list used for checking. Results can be checked in the bottom listviews.

## Further development areas
* A more refined chassis search would narrow down the list used for validation.
* Grouping the results (for example: empty package, double main parts).
* Creating the “bone” for validation: simply build up a virtual package from general parts (for example: screw, seal, O-ring…), define the quantity (not necessary), and then compare how many chassis numbers meet the requirements.

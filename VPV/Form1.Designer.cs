﻿namespace VPV
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Action = new System.Windows.Forms.Label();
            this.lbl_Quantity = new System.Windows.Forms.Label();
            this.lbl_Model = new System.Windows.Forms.Label();
            this.lbl_Factory = new System.Windows.Forms.Label();
            this.lbl_DateFrom = new System.Windows.Forms.Label();
            this.lbl_DateTo = new System.Windows.Forms.Label();
            this.grb_ChassisParameters = new System.Windows.Forms.GroupBox();
            this.lbl_Status = new System.Windows.Forms.Label();
            this.lbl_ChassisResult = new System.Windows.Forms.Label();
            this.btn_ChassisClear = new System.Windows.Forms.Button();
            this.btn_Search = new System.Windows.Forms.Button();
            this.dtp_DateTo = new System.Windows.Forms.DateTimePicker();
            this.dtp_DateFrom = new System.Windows.Forms.DateTimePicker();
            this.cmb_Factory = new System.Windows.Forms.ComboBox();
            this.cmb_Model = new System.Windows.Forms.ComboBox();
            this.cmb_Quantity = new System.Windows.Forms.ComboBox();
            this.cmb_Action = new System.Windows.Forms.ComboBox();
            this.grb_ChassisList = new System.Windows.Forms.GroupBox();
            this.lsv_ChassisList = new System.Windows.Forms.ListView();
            this.grb_Results = new System.Windows.Forms.GroupBox();
            this.lsv_PartList = new System.Windows.Forms.ListView();
            this.lsv_PackageList = new System.Windows.Forms.ListView();
            this.grb_CheckParameters = new System.Windows.Forms.GroupBox();
            this.btn_CheckClear = new System.Windows.Forms.Button();
            this.btn_Check = new System.Windows.Forms.Button();
            this.grb_ChassisParameters.SuspendLayout();
            this.grb_ChassisList.SuspendLayout();
            this.grb_Results.SuspendLayout();
            this.grb_CheckParameters.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_Action
            // 
            this.lbl_Action.AutoSize = true;
            this.lbl_Action.Location = new System.Drawing.Point(6, 27);
            this.lbl_Action.Name = "lbl_Action";
            this.lbl_Action.Size = new System.Drawing.Size(40, 13);
            this.lbl_Action.TabIndex = 0;
            this.lbl_Action.Text = "Action:";
            // 
            // lbl_Quantity
            // 
            this.lbl_Quantity.AutoSize = true;
            this.lbl_Quantity.Location = new System.Drawing.Point(184, 27);
            this.lbl_Quantity.Name = "lbl_Quantity";
            this.lbl_Quantity.Size = new System.Drawing.Size(49, 13);
            this.lbl_Quantity.TabIndex = 1;
            this.lbl_Quantity.Text = "Quantity:";
            // 
            // lbl_Model
            // 
            this.lbl_Model.AutoSize = true;
            this.lbl_Model.Location = new System.Drawing.Point(6, 28);
            this.lbl_Model.Name = "lbl_Model";
            this.lbl_Model.Size = new System.Drawing.Size(39, 13);
            this.lbl_Model.TabIndex = 2;
            this.lbl_Model.Text = "Model:";
            // 
            // lbl_Factory
            // 
            this.lbl_Factory.AutoSize = true;
            this.lbl_Factory.Location = new System.Drawing.Point(184, 28);
            this.lbl_Factory.Name = "lbl_Factory";
            this.lbl_Factory.Size = new System.Drawing.Size(45, 13);
            this.lbl_Factory.TabIndex = 3;
            this.lbl_Factory.Text = "Factory:";
            // 
            // lbl_DateFrom
            // 
            this.lbl_DateFrom.AutoSize = true;
            this.lbl_DateFrom.Location = new System.Drawing.Point(6, 58);
            this.lbl_DateFrom.Name = "lbl_DateFrom";
            this.lbl_DateFrom.Size = new System.Drawing.Size(56, 13);
            this.lbl_DateFrom.TabIndex = 4;
            this.lbl_DateFrom.Text = "Date from:";
            // 
            // lbl_DateTo
            // 
            this.lbl_DateTo.AutoSize = true;
            this.lbl_DateTo.Location = new System.Drawing.Point(184, 58);
            this.lbl_DateTo.Name = "lbl_DateTo";
            this.lbl_DateTo.Size = new System.Drawing.Size(45, 13);
            this.lbl_DateTo.TabIndex = 5;
            this.lbl_DateTo.Text = "Date to:";
            // 
            // grb_ChassisParameters
            // 
            this.grb_ChassisParameters.Controls.Add(this.lbl_Status);
            this.grb_ChassisParameters.Controls.Add(this.lbl_ChassisResult);
            this.grb_ChassisParameters.Controls.Add(this.btn_ChassisClear);
            this.grb_ChassisParameters.Controls.Add(this.btn_Search);
            this.grb_ChassisParameters.Controls.Add(this.dtp_DateTo);
            this.grb_ChassisParameters.Controls.Add(this.dtp_DateFrom);
            this.grb_ChassisParameters.Controls.Add(this.lbl_DateTo);
            this.grb_ChassisParameters.Controls.Add(this.cmb_Factory);
            this.grb_ChassisParameters.Controls.Add(this.lbl_DateFrom);
            this.grb_ChassisParameters.Controls.Add(this.cmb_Model);
            this.grb_ChassisParameters.Controls.Add(this.lbl_Factory);
            this.grb_ChassisParameters.Controls.Add(this.lbl_Model);
            this.grb_ChassisParameters.Location = new System.Drawing.Point(11, 12);
            this.grb_ChassisParameters.Name = "grb_ChassisParameters";
            this.grb_ChassisParameters.Size = new System.Drawing.Size(354, 150);
            this.grb_ChassisParameters.TabIndex = 6;
            this.grb_ChassisParameters.TabStop = false;
            this.grb_ChassisParameters.Text = "Chassis Parameters";
            // 
            // lbl_Status
            // 
            this.lbl_Status.AutoSize = true;
            this.lbl_Status.Location = new System.Drawing.Point(6, 118);
            this.lbl_Status.Name = "lbl_Status";
            this.lbl_Status.Size = new System.Drawing.Size(0, 13);
            this.lbl_Status.TabIndex = 16;
            // 
            // lbl_ChassisResult
            // 
            this.lbl_ChassisResult.AutoSize = true;
            this.lbl_ChassisResult.Location = new System.Drawing.Point(6, 118);
            this.lbl_ChassisResult.Name = "lbl_ChassisResult";
            this.lbl_ChassisResult.Size = new System.Drawing.Size(0, 13);
            this.lbl_ChassisResult.TabIndex = 13;
            // 
            // btn_ChassisClear
            // 
            this.btn_ChassisClear.Location = new System.Drawing.Point(184, 87);
            this.btn_ChassisClear.Name = "btn_ChassisClear";
            this.btn_ChassisClear.Size = new System.Drawing.Size(75, 23);
            this.btn_ChassisClear.TabIndex = 8;
            this.btn_ChassisClear.Text = "Clear";
            this.btn_ChassisClear.UseVisualStyleBackColor = true;
            this.btn_ChassisClear.Click += new System.EventHandler(this.btn_ChassisClear_Click);
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(265, 87);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(75, 23);
            this.btn_Search.TabIndex = 7;
            this.btn_Search.Text = "Search";
            this.btn_Search.UseVisualStyleBackColor = true;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // dtp_DateTo
            // 
            this.dtp_DateTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_DateTo.Location = new System.Drawing.Point(239, 52);
            this.dtp_DateTo.Name = "dtp_DateTo";
            this.dtp_DateTo.Size = new System.Drawing.Size(101, 20);
            this.dtp_DateTo.TabIndex = 6;
            // 
            // dtp_DateFrom
            // 
            this.dtp_DateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_DateFrom.Location = new System.Drawing.Point(68, 52);
            this.dtp_DateFrom.Name = "dtp_DateFrom";
            this.dtp_DateFrom.Size = new System.Drawing.Size(101, 20);
            this.dtp_DateFrom.TabIndex = 5;
            this.dtp_DateFrom.Value = new System.DateTime(2014, 1, 1, 20, 47, 0, 0);
            // 
            // cmb_Factory
            // 
            this.cmb_Factory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Factory.FormattingEnabled = true;
            this.cmb_Factory.Location = new System.Drawing.Point(239, 25);
            this.cmb_Factory.Name = "cmb_Factory";
            this.cmb_Factory.Size = new System.Drawing.Size(101, 21);
            this.cmb_Factory.TabIndex = 4;
            // 
            // cmb_Model
            // 
            this.cmb_Model.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Model.FormattingEnabled = true;
            this.cmb_Model.Location = new System.Drawing.Point(68, 25);
            this.cmb_Model.Name = "cmb_Model";
            this.cmb_Model.Size = new System.Drawing.Size(101, 21);
            this.cmb_Model.TabIndex = 3;
            // 
            // cmb_Quantity
            // 
            this.cmb_Quantity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Quantity.FormattingEnabled = true;
            this.cmb_Quantity.Location = new System.Drawing.Point(239, 24);
            this.cmb_Quantity.Name = "cmb_Quantity";
            this.cmb_Quantity.Size = new System.Drawing.Size(101, 21);
            this.cmb_Quantity.TabIndex = 2;
            // 
            // cmb_Action
            // 
            this.cmb_Action.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Action.FormattingEnabled = true;
            this.cmb_Action.Location = new System.Drawing.Point(68, 24);
            this.cmb_Action.Name = "cmb_Action";
            this.cmb_Action.Size = new System.Drawing.Size(101, 21);
            this.cmb_Action.TabIndex = 1;
            // 
            // grb_ChassisList
            // 
            this.grb_ChassisList.Controls.Add(this.lsv_ChassisList);
            this.grb_ChassisList.Location = new System.Drawing.Point(372, 12);
            this.grb_ChassisList.Name = "grb_ChassisList";
            this.grb_ChassisList.Size = new System.Drawing.Size(557, 254);
            this.grb_ChassisList.TabIndex = 7;
            this.grb_ChassisList.TabStop = false;
            this.grb_ChassisList.Text = "Chassis list";
            // 
            // lsv_ChassisList
            // 
            this.lsv_ChassisList.Location = new System.Drawing.Point(6, 19);
            this.lsv_ChassisList.Name = "lsv_ChassisList";
            this.lsv_ChassisList.Size = new System.Drawing.Size(544, 229);
            this.lsv_ChassisList.TabIndex = 0;
            this.lsv_ChassisList.UseCompatibleStateImageBehavior = false;
            // 
            // grb_Results
            // 
            this.grb_Results.Controls.Add(this.lsv_PartList);
            this.grb_Results.Controls.Add(this.lsv_PackageList);
            this.grb_Results.Location = new System.Drawing.Point(12, 272);
            this.grb_Results.Name = "grb_Results";
            this.grb_Results.Size = new System.Drawing.Size(917, 296);
            this.grb_Results.TabIndex = 8;
            this.grb_Results.TabStop = false;
            this.grb_Results.Text = "Results";
            // 
            // lsv_PartList
            // 
            this.lsv_PartList.Location = new System.Drawing.Point(9, 19);
            this.lsv_PartList.Name = "lsv_PartList";
            this.lsv_PartList.Size = new System.Drawing.Size(196, 271);
            this.lsv_PartList.TabIndex = 8;
            this.lsv_PartList.UseCompatibleStateImageBehavior = false;
            this.lsv_PartList.SelectedIndexChanged += new System.EventHandler(this.lsv_PartList_SelectedIndexChanged);
            // 
            // lsv_PackageList
            // 
            this.lsv_PackageList.Location = new System.Drawing.Point(239, 19);
            this.lsv_PackageList.Name = "lsv_PackageList";
            this.lsv_PackageList.Size = new System.Drawing.Size(671, 271);
            this.lsv_PackageList.TabIndex = 1;
            this.lsv_PackageList.UseCompatibleStateImageBehavior = false;
            // 
            // grb_CheckParameters
            // 
            this.grb_CheckParameters.Controls.Add(this.btn_CheckClear);
            this.grb_CheckParameters.Controls.Add(this.btn_Check);
            this.grb_CheckParameters.Controls.Add(this.lbl_Action);
            this.grb_CheckParameters.Controls.Add(this.cmb_Action);
            this.grb_CheckParameters.Controls.Add(this.lbl_Quantity);
            this.grb_CheckParameters.Controls.Add(this.cmb_Quantity);
            this.grb_CheckParameters.Location = new System.Drawing.Point(12, 168);
            this.grb_CheckParameters.Name = "grb_CheckParameters";
            this.grb_CheckParameters.Size = new System.Drawing.Size(354, 98);
            this.grb_CheckParameters.TabIndex = 10;
            this.grb_CheckParameters.TabStop = false;
            this.grb_CheckParameters.Text = "Check Parameters";
            // 
            // btn_CheckClear
            // 
            this.btn_CheckClear.Location = new System.Drawing.Point(184, 60);
            this.btn_CheckClear.Name = "btn_CheckClear";
            this.btn_CheckClear.Size = new System.Drawing.Size(75, 23);
            this.btn_CheckClear.TabIndex = 10;
            this.btn_CheckClear.Text = "Clear";
            this.btn_CheckClear.UseVisualStyleBackColor = true;
            this.btn_CheckClear.Click += new System.EventHandler(this.btn_CheckClear_Click);
            // 
            // btn_Check
            // 
            this.btn_Check.Location = new System.Drawing.Point(265, 60);
            this.btn_Check.Name = "btn_Check";
            this.btn_Check.Size = new System.Drawing.Size(75, 23);
            this.btn_Check.TabIndex = 9;
            this.btn_Check.Text = "Check";
            this.btn_Check.UseVisualStyleBackColor = true;
            this.btn_Check.Click += new System.EventHandler(this.btn_Check_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 582);
            this.Controls.Add(this.grb_CheckParameters);
            this.Controls.Add(this.grb_Results);
            this.Controls.Add(this.grb_ChassisList);
            this.Controls.Add(this.grb_ChassisParameters);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Package Validator";
            this.grb_ChassisParameters.ResumeLayout(false);
            this.grb_ChassisParameters.PerformLayout();
            this.grb_ChassisList.ResumeLayout(false);
            this.grb_Results.ResumeLayout(false);
            this.grb_CheckParameters.ResumeLayout(false);
            this.grb_CheckParameters.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl_Action;
        private System.Windows.Forms.Label lbl_Quantity;
        private System.Windows.Forms.Label lbl_Model;
        private System.Windows.Forms.Label lbl_Factory;
        private System.Windows.Forms.Label lbl_DateFrom;
        private System.Windows.Forms.Label lbl_DateTo;
        private System.Windows.Forms.GroupBox grb_ChassisParameters;
        private System.Windows.Forms.ComboBox cmb_Quantity;
        private System.Windows.Forms.ComboBox cmb_Action;
        private System.Windows.Forms.GroupBox grb_ChassisList;
        private System.Windows.Forms.ListView lsv_ChassisList;
        private System.Windows.Forms.ComboBox cmb_Factory;
        private System.Windows.Forms.ComboBox cmb_Model;
        private System.Windows.Forms.DateTimePicker dtp_DateTo;
        private System.Windows.Forms.DateTimePicker dtp_DateFrom;
        private System.Windows.Forms.Button btn_ChassisClear;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.GroupBox grb_Results;
        private System.Windows.Forms.ListView lsv_PartList;
        private System.Windows.Forms.ListView lsv_PackageList;
        private System.Windows.Forms.GroupBox grb_CheckParameters;
        private System.Windows.Forms.Button btn_CheckClear;
        private System.Windows.Forms.Button btn_Check;
        private System.Windows.Forms.Label lbl_ChassisResult;
        private System.Windows.Forms.Label lbl_Status;
    }
}


﻿using System;

namespace VPV
{
    enum Models
    {
        Select,
        Truck1,
        Truck2
    }

    enum Factories
    {
        Select,
        Europe,
        USA
    }

    class Chassis
    {
        int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        int chassisNumber;

        public int ChassisNumber
        {
            get { return chassisNumber; }
            set { chassisNumber = value; }
        }

        Models model;

        internal Models Model
        {
            get { return model; }
            set { model = value; }
        }

        DateTime buildDate;

        public DateTime BuildDate
        {
            get { return buildDate; }
            set { buildDate = value; }
        }

        Factories factory;

        internal Factories Factory
        {
            get { return factory; }
            set { factory = value; }
        }

        string typeDefiningNote;

        public string TypeDefiningNote
        {
            get { return typeDefiningNote; }
            set { typeDefiningNote = value; }
        }

        string partList;

        public string PartList
        {
            get { return partList; }
            set { partList = value; }
        }

        public Chassis(int chassisNumber, Models model, DateTime buildDate, Factories factory, string typeDefiningNote, string partList)
        {
            //Id = id;
            ChassisNumber = chassisNumber;
            this.model = model;
            BuildDate = buildDate;
            this.factory = factory;
            TypeDefiningNote = typeDefiningNote;
            PartList = partList;
        }

        public override string ToString()
        {
            return model + " - " + chassisNumber;
        }
    }
}

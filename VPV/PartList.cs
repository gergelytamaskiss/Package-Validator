﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VPV
{
    enum Actions
    {
        Select,
        Oil_Filter_replace
    }

    class PartList
    {
        int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        int partListNumber;

        public int PartListNumber
        {
            get { return partListNumber; }
            set { partListNumber = value; }
        }

        string partListDescription;

        public string PartListDescription
        {
            get { return partListDescription; }
            set { partListDescription = value; }
        }

        int position;

        public int Position
        {
            get { return position; }
            set { position = value; }
        }

        int partNumber;

        public int PartNumber
        {
            get { return partNumber; }
            set { partNumber = value; }
        }

        string partDescription;

        public string PartDescription
        {
            get { return partDescription; }
            set { partDescription = value; }
        }

        int quantity;

        public int Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        string typeDefiningNotes;

        public string TypeDefiningNotes
        {
            get { return typeDefiningNotes; }
            set { typeDefiningNotes = value; }
        }

        Actions action;

        internal Actions Action
        {
            get { return action; }
            set { action = value; }
        }

        string actionType;

        public string ActionType
        {
            get { return actionType; }
            set { actionType = value; }
        }


        public PartList(int id, int partListNumber, string partListDescription, int position, int partNumber, string partDescription, int quantity, string typeDefiningNotes, Actions action, string actionType)
        {
            Id = id;
            PartListNumber = partListNumber;
            PartListDescription = partListDescription;
            Position = position;
            PartNumber = partNumber;
            PartDescription = partDescription;
            Quantity = quantity;
            TypeDefiningNotes = typeDefiningNotes;
            this.action = action;
            ActionType = actionType;
        }

        public override string ToString()
        {
            return partListNumber + " - " + partListDescription;
        }

    }
}

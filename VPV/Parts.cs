﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VPV
{
    class Parts
    {
        int chassisNumber;

        public int ChassisNumber
        {
            get { return chassisNumber; }
            set { chassisNumber = value; }
        }

        int partsCount;

        public int PartsCount
        {
            get { return partsCount; }
            set { partsCount = value; }
        }

        public Parts(int chassisNumber, int partsCount)
        {
            ChassisNumber = chassisNumber;
            PartsCount = partsCount;
        }

        public override string ToString()
        {
            return chassisNumber + "-" + partsCount;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VPV
{
    enum Quantity
    {
        Select,
        _10,
        _25,
        _50,
        _100,
        _250,
        _500,
        _1000
    }

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            cmb_Model.DataSource = Enum.GetValues(typeof(Models));
            cmb_Factory.DataSource = Enum.GetValues(typeof(Factories));
            cmb_Quantity.DataSource = Enum.GetValues(typeof(Quantity));
            cmb_Action.DataSource = Enum.GetValues(typeof(Actions));

            lsv_ChassisList.GridLines = true;
            lsv_ChassisList.View = View.Details;
            lsv_ChassisList.FullRowSelect = true;
            lsv_ChassisList.Columns.Add("#", 35);
            lsv_ChassisList.Columns.Add("Chassis number", 90);
            lsv_ChassisList.Columns.Add("Model", 50);
            lsv_ChassisList.Columns.Add("Build date", 88);
            lsv_ChassisList.Columns.Add("Factory", 60);
            lsv_ChassisList.Columns.Add("Type Defining Notes", 110);
            lsv_ChassisList.Columns.Add("Part List", 80);

            lsv_PartList.GridLines = true;
            lsv_PartList.View = View.Details;
            lsv_PartList.FullRowSelect = true;
            lsv_PartList.Columns.Add("#", 35);
            lsv_PartList.Columns.Add("Chassis no", 80);
            lsv_PartList.Columns.Add("Parts", 55);

            lsv_PackageList.GridLines = true;
            lsv_PackageList.View = View.Details;
            lsv_PackageList.FullRowSelect = true;
            lsv_PackageList.Columns.Add("#", 35);
            lsv_PackageList.Columns.Add("Part List Number", 90);
            lsv_PackageList.Columns.Add("List Description", 100);
            lsv_PackageList.Columns.Add("Position", 50);
            lsv_PackageList.Columns.Add("Part Number", 80);
            lsv_PackageList.Columns.Add("Part Description", 110);
            lsv_PackageList.Columns.Add("Quantity", 60);
            lsv_PackageList.Columns.Add("Action Type", 80);

            try
            {
                DatabaseManager.ConnectionOpen(ConfigurationManager.ConnectionStrings["VPV.Properties.Settings.Database1ConnectionString"].ConnectionString);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not connect to database: " + ex.Message + "\n" + "Please contact your administrator!", "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        #region Refresh listviews
        public void RefreshChassisListView()
        {
            try
            {
                int i = 1;
                lsv_ChassisList.BeginUpdate();
                lsv_ChassisList.Items.Clear();

                foreach (var item in DatabaseManager.GetChassis())
                {
                    ListViewItem row = new ListViewItem(i.ToString());
                    row.SubItems.Add(item.ChassisNumber.ToString());
                    row.SubItems.Add(item.Model.ToString());
                    row.SubItems.Add(item.BuildDate.ToString());
                    row.SubItems.Add(item.Factory.ToString());
                    row.SubItems.Add(item.TypeDefiningNote);
                    row.SubItems.Add(item.PartList);
                    lsv_ChassisList.Items.Add(row);
                    i++;

                    //Chassis chassisListViewObj = new Chassis(item.Id, item.ChassisNumber, item.ChassisModel, item.BuildDate, item.Factory, item.EngineTdn.ToString(), item.OilFilterPl);
                    //row.Tag = chassisListViewObj;
                }
                lsv_ChassisList.EndUpdate();
                lsv_ChassisList.Refresh();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void RefreshPartListView()
        {
            try
            {
                int i = 1;
                lsv_PartList.BeginUpdate();
                lsv_PartList.Items.Clear();

                foreach (var item in DatabaseManager.GetParts())
                {
                    ListViewItem row = new ListViewItem(i.ToString());
                    row.SubItems.Add(item.ChassisNumber.ToString());
                    row.SubItems.Add(item.PartsCount.ToString());
                    lsv_PartList.Items.Add(row);
                    i++;

                    //Chassis chassisListViewObj = new Chassis(item.Id, item.ChassisNumber, item.ChassisModel, item.BuildDate, item.Factory, item.EngineTdn.ToString(), item.OilFilterPl);
                    //row.Tag = chassisListViewObj;
                }
                lsv_PartList.EndUpdate();
                lsv_PartList.Refresh();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void RefreshPackageListView()
        {
            try
            {
                int i = 1;
                lsv_PackageList.BeginUpdate();
                lsv_PackageList.Items.Clear();

                foreach (var item in DatabaseManager.GetPackage())
                {
                    ListViewItem row = new ListViewItem(i.ToString());
                    row.SubItems.Add(item.PartListNumber.ToString());
                    row.SubItems.Add(item.PartListDescription);
                    row.SubItems.Add(item.Position.ToString());
                    row.SubItems.Add(item.PartNumber.ToString());
                    row.SubItems.Add(item.PartDescription);
                    row.SubItems.Add(item.Quantity.ToString());
                    row.SubItems.Add(item.ActionType.ToString());
                    lsv_PackageList.Items.Add(row);
                    i++;

                    //Chassis chassisListViewObj = new Chassis(item.Id, item.ChassisNumber, item.ChassisModel, item.BuildDate, item.Factory, item.EngineTdn.ToString(), item.OilFilterPl);
                    //row.Tag = chassisListViewObj;
                }
                lsv_PackageList.EndUpdate();
                lsv_PackageList.Refresh();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Clear buttons
        private void btn_ChassisClear_Click(object sender, EventArgs e)
        {
            cmb_Model.SelectedIndex = 0;
            cmb_Factory.SelectedIndex = 0;
            lbl_ChassisResult.Text = string.Empty;

            //DatabaseManager.EmptyChassisList();
            RefreshChassisListView();
        }

        private void btn_CheckClear_Click(object sender, EventArgs e)
        {
            cmb_Action.SelectedIndex = 0;
            cmb_Quantity.SelectedIndex = 0;

            //DatabaseManager.EmptyPartList();
            RefreshPartListView();
        }
        #endregion

        private void btn_Search_Click(object sender, EventArgs e)
        {
            int searchModel = cmb_Model.SelectedIndex;
            int searchFactory = cmb_Factory.SelectedIndex;

            DateTime searchDateFrom = dtp_DateFrom.Value;
            DateTime searchDateTo = dtp_DateTo.Value;

            DatabaseManager.SearchChassis(searchModel, searchFactory, searchDateFrom, searchDateTo);
            RefreshChassisListView();
            lbl_Status.Text = "Search returned " + lsv_ChassisList.Items.Count + " chassis.";
        }

        private void btn_Check_Click(object sender, EventArgs e)
        {
            Variables.selectedAction = cmb_Action.SelectedIndex;

            switch (cmb_Quantity.SelectedIndex)
            {
                case 0:
                    Variables.selectedQuantity = 0;
                    break;
                case 1:
                    Variables.selectedQuantity = 10;
                    break;
                case 2:
                    Variables.selectedQuantity = 25;
                    break;
                case 3:
                    Variables.selectedQuantity = 50;
                    break;
                case 4:
                    Variables.selectedQuantity = 100;
                    break;
                case 5:
                    Variables.selectedQuantity = 250;
                    break;
                case 6:
                    Variables.selectedQuantity = 500;
                    break;
                case 7:
                    Variables.selectedQuantity = 1000;
                    break;
                default:
                    Variables.selectedQuantity = 0;
                    break;
            }

            DatabaseManager.ShuffleList();
            
            DatabaseManager.CreateSelectedPartList();
            RefreshPartListView();
        }

        private void lsv_PartList_SelectedIndexChanged(object sender, EventArgs e)
        {
            Variables.selectedItemIndex = lsv_PartList.FocusedItem.Index;

            DatabaseManager.ListPackage();
            RefreshPackageListView();
        }


    }
}

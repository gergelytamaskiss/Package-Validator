﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VPV
{
    class Packages
    {
        int partListNumber;

        public int PartListNumber
        {
            get { return partListNumber; }
            set { partListNumber = value; }
        }

        string partListDescription;

        public string PartListDescription
        {
            get { return partListDescription; }
            set { partListDescription = value; }
        }

        int position;

        public int Position
        {
            get { return position; }
            set { position = value; }
        }

        int partNumber;

        public int PartNumber
        {
            get { return partNumber; }
            set { partNumber = value; }
        }

        string partDescription;

        public string PartDescription
        {
            get { return partDescription; }
            set { partDescription = value; }
        }

        int quantity;

        public int Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        string actionType;

        public string ActionType
        {
            get { return actionType; }
            set { actionType = value; }
        }

        public Packages(int partListNumber, string partListDescription, int position, int partNumber, string partDescription, int quantity, string actionType)
        {
            PartListNumber = partListNumber;
            PartListDescription = partListDescription;
            Position = position;
            PartNumber = partNumber;
            PartDescription = partDescription;
            Quantity = quantity;
            ActionType = actionType;
        }

        public override string ToString()
        {
            return partListNumber + " - " + partListDescription;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace VPV
{
    static class DatabaseManager
    {
        static SqlConnection connection = new SqlConnection();
        static SqlCommand command = new SqlCommand();

        static List<Chassis> chassisList = new List<Chassis>();

        static List<int> shuffledChassisList = new List<int>();

        static List<Parts> tempPartList = new List<Parts>();
        static List<Parts> partList = new List<Parts>();

        static List<Packages> package = new List<Packages>();

        #region Connection
        public static void ConnectionOpen(string connStr)
        {
            try
            {
                connection.ConnectionString = connStr;
                connection.Open();
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Connecting to database failed!", ex.Message);
            }
        }


        public static void ConnectionClose()
        {
            try
            {
                connection.Close();
                command.Dispose();
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Disconecting from database failed!", ex.Message);
            }
        }
        #endregion

        #region Return lists
        public static List<Chassis> GetChassis()
        {
            return chassisList;
        }

        public static List<Parts> GetParts()
        {
            return partList;
        }

        public static List<Packages> GetPackage()
        {
            return package;
        }
        #endregion

        #region Empty list

        public static void EmptyChassisList()
        {
            if (chassisList.Count > 0)
            {
                chassisList.Clear();
            }
        }

        public static void EmptyPartList()
        {
            if (partList.Count > 0)
            {
                partList.Clear();
            }
        }

        #endregion

        #region ShuffleList

        public static void ShuffleList()
        {
            chassisList.Shuffle();

            if (shuffledChassisList.Count > 0)
            {
                shuffledChassisList.Clear();
            }
            else
            {
                for (int i = 0; i < Variables.selectedQuantity; i++)
                {
                    shuffledChassisList.Add(chassisList[i].ChassisNumber);
                }
            }
        }

        private static Random rng = new Random();

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
        #endregion

        #region Search for chassis
        public static List<Chassis> SearchChassis(int searchModel, int searchFactory, DateTime searchDateFrom, DateTime searchDateTo)
        {
            try
            {
                command.Connection = connection;
                command.CommandText = "SEARCH";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter paramModel = new SqlParameter
                {
                    ParameterName = "@model",
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    Value = searchModel
                };

                SqlParameter paramFactory = new SqlParameter
                {
                    ParameterName = "@factory",
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    Value = searchFactory
                };

                SqlParameter paramDateFrom = new SqlParameter
                {
                    ParameterName = "@datefrom",
                    SqlDbType = SqlDbType.Date,
                    Direction = ParameterDirection.Input,
                    Value = searchDateFrom
                };

                SqlParameter paramDateTo = new SqlParameter
                {
                    ParameterName = "@dateto",
                    SqlDbType = SqlDbType.Date,
                    Direction = ParameterDirection.Input,
                    Value = searchDateTo
                };

                if (chassisList.Count > 0)
                {
                    chassisList.Clear();
                }
                //for (int i = chassisList.Count - 1; i >= 0; i--)
                //{
                //    chassisList.RemoveAt(i);
                //}

                command.Parameters.Add(paramModel);
                command.Parameters.Add(paramFactory);
                command.Parameters.Add(paramDateFrom);
                command.Parameters.Add(paramDateTo);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    chassisList.Add(new Chassis((int)reader["chassis_number"], (Models)(int)reader["model"], (DateTime)reader["build_date"], (Factories)(int)reader["factory"], reader["type_defining_note"].ToString(), reader["part_list"].ToString()));
                }
                reader.Close();
                command.Parameters.Clear();
                return chassisList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        public static void CreateSelectedPartList()
        {
            try
            {
                command.Connection = connection;
                command.CommandText = "LISTALLPARTS";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter paramAction = new SqlParameter
                {
                    ParameterName = "@action",
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    Value = Variables.selectedAction
                };

                if (tempPartList.Count > 0)
                {
                    tempPartList.Clear();
                }

                command.Parameters.Add(paramAction);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    tempPartList.Add(new Parts((int)reader["chassis_number"], (int)reader["parts"]));
                }
                reader.Close();
                command.Parameters.Clear();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            var result = tempPartList.Where(p => shuffledChassisList.Contains(p.ChassisNumber))
                 .ToList();

            if (partList.Count > 0)
            {
                partList.Clear();
            }
            else
            {
                partList.AddRange(result);
            }
        }

        public static void ListPackage()
        {
            Variables.selectedChassisNumber = partList[Variables.selectedItemIndex].ChassisNumber;

            try
            {
                command.Connection = connection;
                command.CommandText = "PACKAGE";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter paramChassisNumber = new SqlParameter
                {
                    ParameterName = "@chassisnumber",
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    Value = Variables.selectedChassisNumber
                };

                SqlParameter paramAction = new SqlParameter
                {
                    ParameterName = "@action",
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    Value = Variables.selectedAction
                };

                if (package.Count > 0)
                {
                    package.Clear();
                }

                command.Parameters.Add(paramChassisNumber);
                command.Parameters.Add(paramAction);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    package.Add(new Packages((int)reader["part_list_number"], reader["part_list_description"].ToString(), (int)reader["position"], (int)reader["part_number"], reader["part_description"].ToString(), (int)reader["quantity"], reader["action_type"].ToString()));
                }
                reader.Close();
                command.Parameters.Clear();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
